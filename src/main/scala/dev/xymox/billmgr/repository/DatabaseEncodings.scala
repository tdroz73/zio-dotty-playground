package dev.xymox.billmgr.repository

import java.time.Instant
import java.util.Date

import io.getquill.MappedEncoding

object DatabaseEncodings {
  implicit val instantEncoder: MappedEncoding[Instant, Date] = MappedEncoding[Instant, Date](Date.from)
  implicit val instantDecoder: MappedEncoding[Date, Instant] = MappedEncoding[Date, Instant](_.toInstant)
}