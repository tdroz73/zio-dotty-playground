package dev.xymox.billmgr.repository

import cats.effect.Blocker
import dev.xymox.billmgr.config.Configuration
import dev.xymox.billmgr.config.Configuration.DbConfig
import doobie.hikari.HikariTransactor
import zio.{Has, RLayer, Task, ZIO, ZLayer, ZManaged}
import zio.blocking.Blocking
import zio.interop.catz._

type DbTransactor = Has[DbTransactor.Resource]

object DbTransactor {
  trait Resource {
    val xa: HikariTransactor[Task]
  } 
  
  val transactorLayer: RLayer[Has[DbConfig], Has[HikariTransactor[Task]]] = ZLayer.fromServiceManaged { dbConfig =>
    val results = for {
      ce <- ZIO.descriptor.map(_.executor.asEC).toManaged_
      blocker = Blocker.liftExecutionContext(Blocking.Service.live.blockingExecutor.asEC)
    } yield HikariTransactor.newHikariTransactor[Task](
        driverClassName = dbConfig.driver,
        user = dbConfig.user,
        pass = dbConfig.password,
        url = dbConfig.url,
        connectEC = ce,
        blocker = blocker
      )
      results.mapM { xa => 
        xa.allocated.map {
          case (transactor, cleanupM) => ZManaged.make(ZIO.succeed(transactor))(_ => cleanupM.orDie)
        }
      }
    }
//  val postgres = {
//    val transactor = for {
//      cfg <- Configuration.live
//      
//    } yield ()
//    
//  }
  
  val live: RLayer[Has[DbConfig], DbTransactor] = transactorLayer >>> ZLayer.fromService { transactor =>
    new Resource {
      override val xa: HikariTransactor[Task] = transactor
    }
  }
}
