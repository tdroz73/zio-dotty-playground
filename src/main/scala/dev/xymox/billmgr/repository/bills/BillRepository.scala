package dev.xymox.billmgr.repository.bills

import java.time.Instant

import zio.{Has, Task}

import dev.xymox.billmgr.repository._

type BillRepository = Has[DbTransactor]

case class Bill(id: Long, payeeId: Long, dayOfMonthDue: Int, minAmount: Double, createdAt: Instant, updatedAt: Option[Instant])

object BillRepository {
  trait Service {
    def allBills: Task[Seq[Bill]]
  }
}
