package dev.xymox.billmgr.config

import dev.xymox.billmgr.config.Configuration.DbConfig
import pureconfig._
import zio._


type Configuration = Has[DbConfig]

object Configuration {
  import pureconfig.generic.auto._

  final case class DbConfig(driver: String, url: String, user: String, password: String)
  
  final case class AppConfig(dbConfig: DbConfig)
  
  val live: ULayer[Configuration] = ZIO.effect(ConfigSource.default.loadOrThrow[AppConfig]).map(c => Has(c.dbConfig)).orDie.toLayerMany
}
