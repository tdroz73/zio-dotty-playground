val zioVersion = "1.0.1"
val doobieVersion = "0.9.0"
val calibanVersion = "0.9.1"

enablePlugins(FlywayPlugin)

flywayUser := "root"
flywayPassword := "p4ssW0rd!"
flywayUrl := "jdbc:postgresql://localhost:5432/finances"

lazy val root = project
  .in(file("."))
  .settings(
    inThisBuild(
      List(
        name := "zio-dotty-playground",
        organization := "dev.xymox",
        version := "0.1.0",
        scalaVersion := "0.26.0-RC1"
      )
    ),
    addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1"),
    libraryDependencies ++= Seq(
      "dev.zio"               %% "zio"               % zioVersion,
      "dev.zio"               %% "zio-test"          % zioVersion % Test,
      "dev.zio"               %% "zio-test-sbt"      % zioVersion % Test,
      "dev.zio"               %% "zio-test-junit"    % zioVersion % Test,
      "dev.zio"               %% "zio-test-magnolia" % zioVersion % Test,
      "org.tpolecat"          %% "doobie-core"       % doobieVersion,
      "org.tpolecat"          %% "doobie-postgres"   % doobieVersion,
      "org.tpolecat"          %% "doobie-hikari"     % doobieVersion,
      "org.tpolecat"          %% "doobie-quill"      % doobieVersion,
      "com.github.pureconfig" %% "pureconfig"        % "0.13.0",
      "com.github.ghostdogpr" %% "caliban"           % calibanVersion
    ),
    libraryDependencies := libraryDependencies.value
      .map(_.withDottyCompat(scalaVersion.value)),
    testFrameworks := Seq(new TestFramework("zio.test.sbt.ZTestFramework"))
  )
